const Telegraf = require('telegraf');
const config = require('config');
const WavesAPI = require('waves-api');
const restify = require('restify');
const Extra = require('telegraf/extra')

var server = restify.createServer();
server.listen(process.env.port || process.env.PORT || 8002, function () {
    console.log('%s listening to %s', server.name, server.url);
});

const Waves = WavesAPI.create(WavesAPI.TESTNET_CONFIG);
const wavesSeed = config.get("wavesSeed");
const seed = Waves.Seed.fromExistingPhrase(wavesSeed);
const assetId = config.get("wavesAssetId");
const assetName = config.get("wavesAssetName");
const assetPrecision = config.get("wavesAssetPrecision");
const botAddress = config.get("wavesBotAddress");

const token = config.get("telegramToken");
const bot = new Telegraf(token);
let currentAddress = null;

bot.use((ctx, next) => {
const start = new Date()
return next().then(() => {
    const ms = new Date() - start
    console.log('response time %sms', ms)
})
})

function launchMenu(ctx){
    ctx.reply('Choose the option:', Extra.HTML().markup((m) =>
    m.inlineKeyboard([
      [m.callbackButton('📪 Address', 'address'),
       m.callbackButton('📓 About', 'about')],
      [m.callbackButton('💸 Add Tokens', 'transferTokens'),
       m.callbackButton('🌊 Add Waves', 'transferWaves')],
      [m.callbackButton('🏦 Your Balance', 'userBalance'),
       m.callbackButton('🤖 Bot Balance', 'botBalance')],
    ])));
}

bot.start((ctx) => {
    ctx.reply('Welcome to the Token Transfer Bot!');
    launchMenu(ctx);
})

bot.command('menu', (ctx) => {
    launchMenu(ctx);
});

bot.command('help', (ctx) => {
    launchMenu(ctx);
});

bot.action('about', ctx => {
    ctx.reply('Test Bot for Waves | Nikita Makarov | 2018');
});

bot.action('address', ctx => {
    if (currentAddress == null){
        ctx.reply('No current address.');
    }
    else{
        ctx.reply(`Current address is ${currentAddress}`);
    }
    ctx.reply('Enter \"address <address> \" to add address.');
});

bot.action('transferTokens', (ctx) => {
    ctx.reply(`Token name: ${assetName}`)
    ctx.reply('Enter \"transfer <value>\" to transfer tokens.');
});

bot.action('transferWaves', (ctx) => {
    ctx.reply('Enter \"transfer <value> waves\" to transfer waves.');
});

bot.action('userBalance', (ctx) => {
    ctx.reply('Enter \"balance wallet\" to get your balance.');
});

bot.action('botBalance', (ctx) => {
    ctx.reply('Enter \"balance bot\" to get bot balance.');
});

bot.hears(/balance (.+)/, ({ match, reply }) => {
    if (currentAddress == null) {
        return reply('First you should enter the wallet address in the menu!');
    }
    let address =  match[1] == 'bot' ? botAddress : currentAddress;
    Waves.API.Node.v1.addresses.balance(String(address)).then((balance) => {
        console.log(balance);
        return reply('The balance: '+balance.balance*Math.pow(10,-8)+' WAVES');
    }).catch(
        (err) => {
            return reply('Wrong address');
            return;
        }
    );
});

bot.hears(/address (.+)/, ({ match, reply }) => {
    currentAddress = match[1];
    return reply(`Address ${currentAddress} successfully added.`);
});

bot.hears(/transfer (.+)/, ({ match, reply }) => {
    if (currentAddress == null) {
        return reply('First you should enter the wallet address in the menu!');
    }
    let amount = match[1].split(" ")[0];

    let currentAssetId = assetId;
    let currentAssetName = assetName;
    let currentAssetPrecision = assetPrecision;

    if (match[1].split(" ")[1]){
        currentAssetId = 'WAVES';
        currentAssetName = 'Wave';
        currentAssetPrecision = 8;
        if (amount > 1) {
            return reply('Please enter the amount less or equal 0.1 wave!');
        }
    }
    else{
        if (amount > 100) {
            return reply(`Please enter the amount less or equal 100 ${currentAssetName}!`);
        }
    }

    const transferData = {
        recipient: currentAddress,
        assetId: currentAssetId,
        amount: Number(amount*Math.pow(10, currentAssetPrecision)),
        feeAssetId: 'WAVES',
        fee: 100000,
        attachment: '',
        timestamp: Date.now()
    };

    return Waves.API.Node.v1.assets.transfer(transferData, seed.keyPair).then((responseData) => { 
        let value = responseData.amount/Math.pow(10, currentAssetPrecision);
        return reply(`Successful! ${value} ${currentAssetName}s has been sent to the current address.`);
    }).catch(
        (err) => {
            console.log(err);
            return reply('Error in transfer.\nCheck your input data.');
        }
    );
});

bot.startPolling();
